import sqlite3
import create as cr

connection = sqlite3.connect("rooms.db")

cursor = connection.cursor()

#Used for debugging
cursor.execute("SELECT * FROM rooms") 
print("fetchall:")
result = cursor.fetchall() 
for r in result:
    print(r)



now_room = cr.first_room

def change_room(dirs):
    global now_room
    
    cursor.execute(f"SELECT {dirs} FROM rooms WHERE name = '{now_room}'")
    res = cursor.fetchone()
    res_string = res[0]

    if res_string == None:
        print()
        print(f"No door at {dirs}")
        print()
        return None

    cursor.execute(f"SELECT name FROM rooms WHERE room_number = {res_string}") 
    res = cursor.fetchone()
    res_string = res[0]
    now_room = res_string
    print(now_room)

    return now_room


while True:
    chk = True
    while chk == True:

        direction = input('Which direction? use n,e,w,s to navigate ').lower()
        if not direction in ['n','e','w','s']:
            print("Not a valid direction")
        else:
            chk = False
        
    if direction == 'n':
        change_room('n')

    elif direction == 'e':
        change_room('e')

    elif direction == 'w':
        change_room('w')
        
    elif direction == 's':
        change_room('s')

