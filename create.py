import sqlite3
import random

#Create db
connection = sqlite3.connect('rooms.db')

cursor = connection.cursor()

sql_command = "DROP TABLE IF EXISTS rooms;"
cursor.execute(sql_command)

sql_command = """
CREATE TABLE rooms ( 
room_number INTEGER PRIMARY KEY, 
name VARCHAR(20) not null, 
n integer,
e integer,
w integer,
s integer
);"""
cursor.execute(sql_command)

def choose_way(w):

    ways = {
        0: 'n',
        1: 'e',
        2: 'w',
        3: 's'
    }
    return ways[w] 


def choose_way_inv(w):

    ways_inv = {
        0: 's',
        1: 'w',
        2: 'e',
        3: 'n'
    }
    return ways_inv[w]


def check_occupation(occ, previous_room):
    #Check if previous room has a occupied connection

    cursor.execute(f"SELECT {occ} FROM rooms WHERE name = '{previous_room}'")
    res = cursor.fetchone()
    res_string = res[0]
    if res_string != None:
        return True
    else:
        return False


## Dungeon Building
rooms_left =  ['Bathroom','Kitchen','Livingroom','Hall','Garden','Roof','Cellar']
rooms_to_make = len(rooms_left) - 1
rooms_made = 1

#First room
secure_random = random.SystemRandom()
first_room = secure_random.choice( rooms_left )
rooms_left.remove( first_room )
rooms_made = 1

sql_command = f"""INSERT INTO rooms 
                (room_number, name)
                VALUES (NULL, "{first_room}");"""
cursor.execute(sql_command)

#Rest of rooms
while len(rooms_left) > 0:

    #Make second room
    if rooms_made == 1:
        # Choose room
        choosen_room = secure_random.choice( rooms_left )
        rooms_left.remove( choosen_room )

        #Choose direction
        way_rand = secure_random.randint(0,3)
        way = choose_way(way_rand)
        way_inv = choose_way_inv(way_rand)

        #Make sql payload for new room
        sql_command = f"""INSERT INTO rooms 
                    (room_number, name, {way_inv})
                    VALUES (NULL, "{choosen_room}", 1);"""
        cursor.execute(sql_command)

        #Make first room to second connection
        sql_command = f"""UPDATE rooms SET {way} = {rooms_made + 1} WHERE room_number = {rooms_made};"""
        cursor.execute(sql_command)

        #Counter
        rooms_made +=1
        previous_room = choosen_room


    #Make other rooms
    else:
        a = True
        while a == True:

            # Choose room
            choosen_room = secure_random.choice( rooms_left )
            

            #Choose direction
            way_rand = secure_random.randint(0,3)
            way = choose_way(way_rand)
            way_inv = choose_way_inv(way_rand)

            if check_occupation(way, previous_room):
                print("Crash")
            else:
                a = False

        
        #Make sql payload for next room
        sql_command = f"""INSERT INTO rooms 
                    (room_number, name, {way_inv})
                    VALUES (NULL, "{choosen_room}", {rooms_made});"""
        cursor.execute(sql_command)

        #Make previous room connection
        sql_command = f"""UPDATE rooms SET {way} = {rooms_made + 1} WHERE room_number = {rooms_made};"""
        cursor.execute(sql_command)

        #Counter
        rooms_left.remove( choosen_room )
        rooms_made +=1
        previous_room = choosen_room

        

    

'''

# never forget this, if you want the changes to be saved:
'''
connection.commit()

connection.close()
